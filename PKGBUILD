# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

_linuxprefix=linux510-rt
_extramodules=extramodules-5.10-rt-MANJARO
# don't edit here
pkgver=418.113_5.10.78_rt55_1

_nver=418
# edit here for new version
_sver=113
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-${_nver}xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
makedepends=("$_linuxprefix" "$_linuxprefix-headers" "nvidia-${_nver}xx-utils=${_pkgver}")
groups=("$_linuxprefix-extramodules")
provides=("${_pkgname}=${_pkgver}" "nvidia-${_nver}xx-modules=${_pkgver}")
conflicts=("nvidia-${_nver}xx-dkms" "$_linuxprefix-nvidia" "$_linuxprefix-nvidia-340xx" "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-430xx"
           "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx" "$_linuxprefix-nvidia-455xx"
           "$_linuxprefix-nvidia-460xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="http://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run"
        "license.patch" "kernel-5.5.patch" "kernel-5.6.patch" "kernel-5.7.patch"
        "kernel-5.8.patch" "kernel-5.9.patch" "kernel-5.10.patch" "kernel-5.10-rt.patch")
sha256sums=('feb01ca86fa65c01356769f75b744c219224c223ac114c7f0c88ef1531cb9db4'
            '43bc1c1caf0acf0173526520e80fcd555b752e301afe0ffd5311b5dc54f3106f'
            '668b963eb4266fc7142e7d8d0fdca2d9956bad4e286b9e9b09e44b0483b419c8'
            '80c9b1695ff04ef4413aa94d323ecae44eaa26e48680e28da2bbe4d7ed9d1d9b'
            'd0c8358fc8b066e152c470dc8f9741028a31829526dd752ffc9aa80a0b2e639e'
            '3dde474ec7e3e0305186065de78f26600c482bf338c4eaff5f4694b3c9bff8f3'
            '3b13de3905dae74c6c72d0d5be46c6211d459563ee8b39d7698ae8b8242fc5a7'
            'f3d094d98884a9e130eeef48708bf7e3491fe7198562ef1277ec3c44c8f9ead3'
            '3901016a62bdd09f7317fc4db25c720cc3fb6f701408fdf4dbf1bf33d17dffe7')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    # patches here
    # original patches https://www.if-not-true-then-false.com/2020/inttf-nvidia-patcher
    # Fix compile problem with license
    msg2 "PATCH: license"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/license.patch

    # Fix compile problem with 5.5
    msg2 "PATCH: kernel-5.5"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.5.patch

    # Fix compile problem with 5.6
    msg2 "PATCH: kernel-5.6"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.6.patch

    # Fix compile problem with 5.7
    msg2 "PATCH: kernel-5.7"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.7.patch

    # Fix compile problem with 5.8
    msg2 "PATCH: kernel-5.8"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.8.patch

    # Fix compile problem with 5.9
    msg2 "PATCH: kernel-5.9"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.9.patch

    # Fix compile problem with 5.10
    msg2 "PATCH: kernel-5.10"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.10.patch
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.10-rt.patch

    export IGNORE_PREEMPT_RT_PRESENCE=1
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-modeset.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-modeset.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-drm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-drm.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-uvm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
